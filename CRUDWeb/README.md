<h1>CRUD Web</h1>

- Run localhost Xampp `http://localhost/CRUD-Web/` make sure to put the folder in htdocs folder.
- Click create(Green) button to add new data.
- Click refresh(Blue) button to read and display the data.
- Click edit(In table) to edit the data.
- Pen(Black & White) button for update data.
- Delete(Red) button for delete data.
- Delete all to delete all data.

> Note : Still have bugs with ID primary key!
