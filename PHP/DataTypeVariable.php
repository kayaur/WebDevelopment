<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Data Types & Variables</title>
</head>
<body>
     <?php
     // Integer
     $integer_value = 19;
     echo "Number: " . $integer_value . " ";
     echo var_dump($integer_value);

     // Floating
     $floating_value = 10.99;
     echo "<br>Floating: " . $floating_value . " ";
     echo var_dump($floating_value);

     // String
     $string_value = "Yooo World!";
     echo "<br>String: " . $string_value . " ";
     echo var_dump($string_value);

     // Boolean
     $boolean_value = true;
     echo "<br>Boolean: " . $boolean_value . " ";
     echo var_dump($boolean_value);

     if (isset($string_value)) {
          echo "<br> You've already create this variable!";
     }
     else
     {
          echo "<br> Alright you good!";
     }
     ?>
</body>
</html>