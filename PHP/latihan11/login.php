<?php
require 'function.php';

session_start();

if (isset($_SESSION["login"])) {
    header("Location : index.php");

    exit();
}

if (isset($_COOKIE["id"]) && isset($_COOKIE["key"])) {
    $id = $_COOKIE["id"];
    $key = $_COOKIE["key"];

    $result = mysqli_query($conn, "SELECT * FROM users WHERE id = $id");
    $row = mysqli_fetch_assoc($result);

    if ($key === hash('ripemd160', $row["username"])) {
        $_SESSION["login"] = true;
    }
}

if (isset($_POST["submit"])) {
    $un = $_POST["un"];
    $pwd = $_POST["pwd"];
    $result = mysqli_query($conn, "SELECT * FROM users WHERE username='$un'");

    if (mysqli_num_rows($result) === 1) {
        $row = mysqli_fetch_assoc($result);
        if (password_verify($pwd, $row["password"])) {
            header("Location: index.php");
            exit;
        }
    }

    $error = true;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="css/loginstyle.css">
</head>

<body>
    <h1>Login</h1>
    <hr>
    <form action="" method="post">
        <div class="container">
            <label for="un"><b>Username</b></label>
            <input type="text" placeholder="Masukan Username" name="un" required>

            <label for="pwd"><b>Password</b></label>
            <input type="password" placeholder="Masukan Password" name="pwd" required>

            <?php if (isset($error)) : ?>
                <p style="color:red; font-style: italic;">username / password salah!</p>
            <?php endif; ?>

            <button type="submit" name="submit">Login</button>
            <label><input type="checkbox" checked="checked" name="remember"> Remember me</label>
        </div>

        <div class="container" style="background-color:#f1f1f1">
            <button type="button" class="cancelbtn">Cancel</button>
            <span class="psw">Forgot <a href="#">password?</a></span>
        </div>
    </form>
</body>

</html>