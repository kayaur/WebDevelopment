<?php

require "function.php";
$mahasiswa = query("SELECT * FROM mahasiswa");

//cari
if (isset($_POST["cari"])) {
    $mahasiswa = cari($_POST["keyword"]);
}

if (isset($_POST["submit"])) {
    $un = $_POST["un"];
    $pwd = $_POST["pwd"];
    $result = mysqli_query($conn, "SELECT * FROM users WHERE username='$un'");
    // cek username 
    if (mysqli_num_rows($result) === 1) {
        // cek password
        $row = mysqli_fetch_assoc($result);
        if (password_verify($pwd, $row["password"])) {
            header("Location: index.php");
            exit;
        }
    }
    $error = true;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Mahasiswa </title>
</head>

<body>
    <h1>Selamat Datang Admin</h1>
    <a href="logout.php">Log Out</a>
    <h1>Daftar Mahasiswa</h1>
    <hr>
    <a href="insert.php">AddData</a>
    <br>
    <br>
    <form action="" method="post">
        <input type="text" name="keyword" size="50" placeholder="Masukkan Keyword" autofocus>
        <button type="submit" name="cari">Cari</button>
    </form>
    <br>
    <table border="1" cellpadding="10" cellspacing="0">
        <tr>
            <th>No.</th>
            <th>Nama</th>
            <th>NIM</th>
            <th>Jurusan</th>
            <th>Gambar</th>
            <th>Action</th>
        </tr>
        <?php $i = 1; ?>
        <?php foreach ($mahasiswa as $mhs) : ?>
            <tr>
                <td><?= $i; ?></td>
                <td><?= $mhs["nama"]; ?></td>
                <td><?= $mhs["nim"]; ?></td>
                <td><?= $mhs["jurusan"]; ?></td>
                <td>
                    <img src="<?= $mhs['gambar']; ?>" alt="image" width="100px" height="100px">
                </td>
                <td>
                    <a href="update.php?id=<?= $mhs["id"]; ?>">Update</a>
                    <a href="delete.php?id=<?= $mhs["id"]; ?>" onclick="return confirm('Apakah anda yakin?');">Hapus</a>
                </td>
            </tr>
            <?php $i++; ?>
        <?php endforeach; ?>
</body>

</html>