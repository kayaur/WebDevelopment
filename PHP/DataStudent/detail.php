<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Data Mahasiswa</title>
</head>
<body>
     <h1>Data Mahasiswa</h1>

     <hr>

     <a style="text-decoration:none" href="latihan2a.php">Back</a>

     <br>
     <br>
     
     <?php
     if (isset($_GET['name']) && isset($_GET['NIM']) && isset($_GET['address']) && isset($_GET['dateOfBirth']) && isset($_GET['generation']) && isset($_GET['majors']) && isset($_GET['image'])) {
          echo "<li>" . $_GET['name'] . "</li>";
          echo "<li>" . $_GET['NIM'] . "</li>";
          echo "<li>" . $_GET['address'] . "</li>";
          echo "<li>" . $_GET['dateOfBirth'] . "</li>";
          echo "<li>" . $_GET['generation'] . "</li>";
          echo "<li>" . $_GET['majors'] . "</li>";

          $image = $_GET['image'];

          if ($image == "profile1") {
               echo "<li><img src='Assets/billy.png' alt='image'></li>";
          } else if ($image == "profile2") {
               echo "<li><img src='Assets/eren.jpg' alt='image'></li>";
          } else if ($image == "profile3") {
               echo "<li><img src='Assets/levi.jpg' alt='image'></li>";
          }
     } 
     else
     {
          throw new Exception("Data not found!");
     }
     ?>
     </ul>
</body>
</html>