<?php
// Loop string array
$Name = ["Billy", "Franscois"];

foreach ($Name as $item) {
     echo $item . " ";
}

var_dump($Name);

echo "<br>";

// Loop array Numbers
$Number = array(1, 2, 3, 4, 5);

for ($i = 0; $i < count($Number); $i++) {
     echo $Number[$i] . " ";
}

var_dump($Number);
