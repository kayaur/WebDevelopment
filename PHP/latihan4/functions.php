<?php
// Create connection to database
$connect = mysqli_connect("localhost", "root", "", "phpbasic");

// Check connection to database
if ($connect->connect_error) {
     die("Connection failed: " . $connect->connect_error);
}

$sql_data = "INSERT INTO mahasiswa (nama, nim, jurusan, gambar) VALUES ('Billy Franscois', '12345', 'Informatika', 'billy.png')";

if ($connect->Query($sql_data) === TRUE) {
     // echo "New record created successfully!";
} else {
     echo "Error: " . $sql_data . "<br>" . $connect->error;
}

// Delete data from database
$sql_delete = "DELETE FROM mahasiswa WHERE id = 78";

if (mysqli_query($connect, $sql_delete)) {
     // echo "Delete data successfully!";
} else {
     echo "Error: " . $sql_delete . "<br>" . mysqli_error($connect);
}

function Query($query)
{
     global $connect;

     $result = mysqli_query($connect, $query);

     $rows = [];

     while ($row = mysqli_fetch_assoc($result)) {
          $rows[] = $row;
     }

     return $rows;
}

// Add new data to database
function AddData($post)
{
     global $connect;

     $nama = htmlspecialchars($_POST['nama']);
     $nim = htmlspecialchars($_POST['nim']);
     $jurusan = htmlspecialchars($_POST['jurusan']);
     $gambar = htmlspecialchars($_POST['gambar']);

     $query = "INSERT INTO mahasiswa VALUES ('', '$nama', '$nim', '$jurusan', '$gambar')";

     mysqli_query($connect, $query);

     return mysqli_affected_rows($connect);
}
