<?php
require "functions.php";

if (isset($_POST['submit'])) {
     if (AddData($_POST) > 0) {
          echo "<script>
          alert('Add data successfully');
          document.location.href = 'index.php';
          </script>";
     } else {
          echo "<script>
          alert('Failed to add data');
          document.location.href = 'index.php';
          </script>";
     }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Add Data</title>
</head>

<body>
     <h1>Add Data</h1>

     <form action="" method="post">
          <pre>
          <label for="nama">Masukan Nama:</label>
          <input type="text" name="nama" id="nama">

          <label for="nim">NIM:</label>
          <input type="text" name="nim" id="nim">

          <label for="jurusan">Jurusan:</label>
          <input type="text" name="jurusan" id="jurusan">

          <label for="gambar">Gambar:</label>
          <input type="text" name="gambar" id="gambar">

          <button type="submit" name="submit">Add Data</button>    
     </form>
</body>
</html>