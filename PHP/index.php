<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Learning PHP</title>
</head>

<body>
     <h1 style="text-align: center">
          <?php
          // This is the PHP code that will be executed when the page is loaded.
          echo "Hello World!";
          ?>
     </h1>
     <p>
          <?php
          $name = "Billy";
          $num = 19;
          echo $name . " \n";
          echo $num;
          ?>
     </p>
     <p>
          <?php
          for ($i = 1; $i <= 5; $i++) {
               echo $i . " ";
          }
          ?>
     </p>
</body>

</html>