<a href="insert.php">AddData</a>

<?php
require "function.php";

$mahasiswa = query("SELECT * FROM mahasiswa");

if (isset($_POST["cari"])) {
    $mahasiswa = search($_POST["keyword"]);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Mahasiswa </title>
</head>

<body>
    <h1>Daftar Mahasiswa</h1>
    <hr>
    <table borde="1" cellpadding="10" cellspacing="0">
        <tr>
            <th>No.</th>
            <th>Nama</th>
            <th>NIM</th>
            <th>Jurusan</th>
            <th>Gambar</th>
            <th>Action</th>
        </tr>
        <?php $i = 1; ?>
        <?php foreach ($mahasiswa as $mhs) : ?>
            <tr>
                <td><?= $i; ?></td>
                <td><?= $mhs["nama"]; ?></td>
                <td><?= $mhs["nim"]; ?></td>
                <td><?= $mhs["jurusan"]; ?></td>
                <td>
                    <img src="<?= $mhs["gambar"]; ?>" alt="image">
                </td>
                <td>
                    <a href="update.php?id=<?= $mhs["id"]; ?>">Edit</a>
                    <a href="delete.php?id=<?= $mhs["id"]; ?>" onclick="return confirm('Apakah anda yakin?');">Hapus</a>
                </td>
            </tr>
            <?php $i++; ?>
        <?php endforeach; ?>
    </table>

    <br>
    <br>
    <br>

    <form action="" method="post">
        <input type="text" name="keyword" size="50" placeholder="masukan key word" autofocus>
        <button type="submit" name="cari">Cari</button>
    </form>

</body>

</html>