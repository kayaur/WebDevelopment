<?php
class About extends Controller
{
    public function index($name = 'Billy', $job = 'Programmer', $age = 18)
    {
        $data['name'] = $name;
        $data['job'] = $job;
        $data['age'] = $age;

        $this->view('about/index', $data);
    }

    public function page()
    {
        $this->view('about/page');
    }
}
?>