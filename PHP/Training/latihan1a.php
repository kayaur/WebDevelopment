<?php 
$data_student =
     ["name" => "Billy Franscois", 
      "NIM" => "12155201200012",
      "dateOfBirth" => "08-12-1990", 
      "generation" => "2020", 
      "majors" => "Informatika"
     ]
?>

<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Latihan 1A</title>
</head>
<body>
     <h1>Data Mahasiswa</h1>

     <hr>

     <ul>
          <?php 
          foreach ($data_student as $value) : ?>
               <li> <?= $value; ?></li>
          <?php endforeach
          ?>
     </ul>
</body>
</html>