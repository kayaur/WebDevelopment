<?php
// Create connection to database
$connect = mysqli_connect("localhost", "root", "", "phpbasic");

// Check connection to database
if ($connect->connect_error)
{
     die("Connection failed: " . $connect->connect_error);
}

$sql_data = "INSERT INTO mahasiswa (nama, nim, jurusan, gambar) VALUES ('Billy Franscois', '12345', 'Informatika', 'billy.png')";

if ($connect->Query($sql_data) === TRUE)
{
     echo "New record created successfully!";
}
else
{
     echo "Error: " . $sql_data . "<br>" . $connect->error;
}

function Query($query)
{
     global $connect;

     $result = mysqli_query($connect, $query);

     $rows = [];

     while ($row = mysqli_fetch_assoc($result))
     {
          $rows[] = $row;
     }

     return $rows;
}
?>