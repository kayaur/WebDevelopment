<?php
require "function.php";

if (isset($_POST['submit'])) {
     if (AddData($_POST) > 0) {
          echo "<script>
          alert('Add data successfully');
          document.location.href = 'index.php';
          </script>";
     } else {
          echo "<script>
          alert('Failed to add data');
          document.location.href = 'index.php';
          </script>";
     }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>AddData</title>
</head>

<body>
     <h1>AddData</h1>

     <form action="" method="post" enctype="multipart/form-data">
          <pre>
          <label for="nama">Masukan Nama:</label>
          <input type="text" name="nama" id="nama" required>

          <label for="nim">Nim:</label>
          <input type="text" name="nim" id="nim" required>

          <label for="jurusan">Jurusan:</label>
          <input type="text" name="jurusan" id="jurusan"required>

          <label for="gambar">Gambar:</label>
          <input type="file" name="gambar" id="gambar" required>

          <button type="submit" name="submit">Tambah Data</button>    
     </form>
</body>
</html>