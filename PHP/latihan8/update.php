<?php
require "function.php";

$id = $_GET["id"];

$mhs = query("SELECT * FROM mahasiswa WHERE id=$id");

if (isset($_POST["submit"])) {
    if (update($_POST) > 0) {
        echo "<script>
    alert('Data diubah!');
    document.location.href = 'index.php';
    </script>";
    } else {
        echo "<script>
    alert('Data gagal diubah!');
    document.location.href = 'index.php';
    </script>";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Data</title>
</head>

<body>
    <h1>Update Data</h1>
    <hr>
    <from action="" method="post">
        <pre>
            <input type="hidden" name="id" value="<?= $mhs["id"]; ?>">
            <input  type="hidden"  name="gambarLama"  value="<?= $mhs["gambar"]; ?>">

            <label for="nama">Masukan Nama</label>
            <input type="text" name="nama" id="nama" value="<?= $mhs["nama"]; ?>" required>
            
            <label for="nim">Masukan NIM</label>
            <input type="text" name="nim" id="nim" value="<?= $mhs["nim"]; ?>" required>

            <label for="jurusan">Masukan Jurusan</label>
            <input type="text" name="jurusan" id="jurusan" value="<?= $mhs["jurusan"]; ?>" required>
            
            <label for="gambar">Gambar</label>
            <input type="text" name="gambar" id="gambar" value="<?= $mhs["gambar"]; ?>" required>
            <br>
            <button type="submit" name="submit">Update</button>
        </pre>
    </from>
</body>

</html>