<?php

$conn = mysqli_connect("localhost", "Billy", "billy19", "phpbasic");

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

function query($query)
{
    global $conn;

    $result = mysqli_query($conn, $query);

    $rows = [];

    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }

    return $rows;
}

//add new data
function AddData($post)
{
    global $conn;

    $nama = htmlspecialchars($_POST['nama']);
    $nim = htmlspecialchars($_POST['nim']);
    $jurusan = htmlspecialchars($_POST['jurusan']);
    $gambar = htmlspecialchars($_POST['gambar']);

    $gambar = upload();

    if (!$gambar) {
        return false;
    }

    $query = "INSERT INTO mahasiswa VALUES ('', '$nama', '$nim', '$jurusan', '$gambar')";

    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function upload()
{
    $namaFile  =  $_FILES["gambar"]["name"];
    $ukuranFile  =  $_FILES["gambar"]["size"];
    $error  =  $_FILES["gambar"]["error"];
    $ext  =  strtolower(pathinfo($namaFile,  PATHINFO_EXTENSION));
    $tmpName  =  $_FILES["gambar"]["tmp_name"];

    if ($error === 4) {
        echo  "<script> alert('Gambar  harus  diupload!'); </script>";

        return  false;
    }

    //  ekstensi  yang  diijinkan
    $extValid  =  ["jpg",  "jpeg",  "png"];

    if (!in_array($ext,  $extValid)) {
        echo  "<script> alert('File  yang  diupload  hanya  gambar!'); </script>";

        return  false;
    }

    // cek ukuran
    if ($ukuranFile  >  1000000) {
        echo  "<script> alert('Ukuran  file  terlalu  besar!'); </script>";

        return  false;
    }

    //  ubah  nama  file  gambar
    $namaBaru  =  uniqid();
    $namaBaru  .=  ".";
    $namaBaru  .=  $ext;

    //  lolos  validasi
    move_uploaded_file($tmpName,  '/../../gambar/'  .  $namaBaru);

    return  $namaBaru;
}

//delete data
function delete($id)
{
    global $conn;
    $query = "DELETE FROM mahasiswa WHERE id=$id";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

//update data
function update($data)
{
    global $conn;

    $id = $data["id"];
    $nama = htmlspecialchars($data["nama"]);
    $nim = htmlspecialchars($data["nim"]);
    $jurusan = htmlspecialchars($data["jurusan"]);

    $gambarLama = $data["gambarLama"];
    $error = $_FILES["gambar"]["error"];

    $gambar  =  $gambarLama;

    if ($error === 4) {
    } else {
        unlink('/../../gambar/' . $gambarLama);
        $gambar  =  upload();
    }

    $query = "UPDATE mahasiswa SET nama = '$nama', nim = '$nim', jurusan = '$jurusan', gambar = '$gambar' WHERE id = $id";

    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function search($keyword)
{
    $query = "SELECT * FROM mahasiswa WHERE nama LIKE '%$keyword%' OR nim LIKE '%$keyword%' OR jurusan LIKE '%$keyword%'";

    return query($query);
}
