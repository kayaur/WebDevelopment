-- Create database
CREATE DATABASE crud_pdo;

-- Create table (MYSQL)
CREATE TABLE students(
     ID INT NOT NULL AUTO_INCREMENT,
     Name VARCHAR(100) NOT NULL,
     NIM VARCHAR(20) NOT NULL,
     Email VARCHAR(200) NOT NULL,
     Campus VARCHAR(100) NOT NULL,
     Programs VARCHAR(100) NOT NULL,

     PRIMARY KEY(ID)
);

-- Create table (SQL Server)
CREATE TABLE [students](
     [ID] INT NOT NULL IDENTITY(1,1),
     [Name] VARCHAR(100) NOT NULL,
     [NIM] VARCHAR(20) NOT NULL,
     [Email] VARCHAR(200) NOT NULL,
     [Campus] VARCHAR(100) NOT NULL,
     [Programs] VARCHAR(100) NOT NULL,

     PRIMARY KEY(ID)
);

SELECT * FROM crud_pdo.students;

-- Adding new column
ALTER TABLE `students` 
	ADD `Profile` VARCHAR(250) NOT NULL
    AFTER `Programs`;

-- Remove column
ALTER TABLE `students` 
     DROP `Profile`;