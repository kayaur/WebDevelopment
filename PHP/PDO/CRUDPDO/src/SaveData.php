<?php
if (isset($_POST['Save'])) {
     require_once("DatabaseConfigurationConnection.php");

     $connection = new DatabaseConfigurationConnection();

     $connection->CreateConnection();

     $connection->SetName($_POST['Name']);
     $connection->SetNIM($_POST['NIM']);
     $connection->SetEmail($_POST['Email']);
     $connection->SetCampus($_POST['Campus']);
     $connection->SetPrograms($_POST['Programs']);

     $connection->Save();
}
