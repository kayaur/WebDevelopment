<?php
require_once("DatabaseConfigurationConnection.php");

$connect = new DatabaseConfigurationConnection();

$connect->CreateConnection();

if (isset($_GET['ID']) && isset($_GET['request']) && $_GET['request'] == 'delete') {
     $connect->SetID($_GET['ID']);

     $connect->Delete();
}
