<?php
class DatabaseConfigurationConnection
{
     // MYSQL Connection properties
     private $dbDSN = 'mysql:host=localhost;dbname=crud_pdo';
     private $dbUsername = 'BillyFrcs';
     private $dbPassword = 'billy';

     private $connect;

     // Input properties
     private $ID;
     private $Name;
     private $NIM;
     private $Email;
     private $Campus;
     private $Programs;

     private $options = [
          PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
          PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
     ];

     public function __construct()
     {
          try {
               define('DB_DSN', $this->dbDSN);
               define('DB_USERNAME', $this->dbUsername);
               define('DB_PASSWORD', $this->dbPassword);
               define('DB_OPTIONS', $this->options);

               $this->connect = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD, DB_OPTIONS);
          } catch (PDOException $error) {
               die("Yoo there was an error while connecting to the server " . $error->getMessage());
          }
     }

     // Create a connection object to the database
     public function CreateConnection()
     {
          try {
               $this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

               echo '<script>console.log("Connected successfully, just enjoy your life!");</script>';
          } catch (PDOException $error) {
               die("Yoo there was an error while connecting to the server " . $error->getMessage());

               exit;
          }
     }

     public function Save()
     {
          try {
               $sql = $this->connect->prepare("INSERT INTO crud_pdo.students(Name, NIM, Email, Campus, Programs) VALUES (?, ?, ?, ?, ?)");

               $sql->execute(array($this->Name, $this->NIM, $this->Email, $this->Campus, $this->Programs));

               echo '<script>alert("Added Data Successfully!"); document.location.href="index.php"</script>';
          } catch (PDOException $error) {
               die("Yoo there was an error while connecting to the server " . $error->getMessage());
          }
     }

     public function FetchAll()
     {
          try {
               $sql = $this->connect->prepare("SELECT * FROM crud_pdo.students");

               $sql->execute();

               return $sql->fetchAll();
          } catch (PDOException $error) {
               return "Yoo there was an error while connecting to the server " . $error->getMessage();
          }
     }

     public function FetchOne()
     {
          try {
               $sql = $this->connect->prepare("SELECT * FROM crud_pdo.students WHERE ID = ?");

               $sql->execute(array($this->ID));

               return $sql->fetchAll();
          } catch (PDOException $error) {
               return "Yoo there was an error while connecting to the server " . $error->getMessage();
          }
     }

     public function Update()
     {
          try {
               $sql = $this->connect->prepare("UPDATE crud_pdo.students SET Name = ?, NIM = ?, Email = ?, Campus = ?, Programs = ? WHERE ID = ?");

               $sql->execute(array($this->Name, $this->NIM, $this->Email, $this->Campus, $this->Programs, $this->ID));

               echo '<script>alert("Update Data Successfully!"); document.location.href="index.php"</script>';
          } catch (PDOException $error) {
               die("Yoo there was an error while connecting to the server " . $error->getMessage());
          }
     }

     public function Delete()
     {
          try {
               $sql = $this->connect->prepare("DELETE FROM crud_pdo.students WHERE ID = ?");

               $sql->execute(array($this->ID));

               $sql->fetchAll();

               echo '<script>alert("Data Deleted Successfully!"); document.location.href="index.php"</script>';
          } catch (PDOException $error) {
               die("Yoo there was an error while connecting to the server " . $error->getMessage());
          }
     }

     public function Search($keyword)
     {
          try {
               // MYSQL
               $sql = $this->connect->prepare("SELECT * FROM crud_pdo.students WHERE Name LIKE :Keyword OR NIM LIKE :Keyword OR Email LIKE :Keyword ORDER BY Name");
               $sql->bindValue(':Keyword', '%' . $keyword . '%', PDO::PARAM_STR);

               $sql->execute();

               $resultSearch = $sql->fetchAll();

               return $resultSearch;
          } catch (PDOException $error) {
               return ("Yoo there was an error while connecting to the server " . $error->getMessage());
          }
     }

     public function CheckRows()
     {
          try {
               $sql = $this->connect->prepare("SELECT * FROM crud_pdo.students");

               $sql->execute();

               return $sql->rowCount();
          } catch (PDOException $error) {
               return "Yoo there was an error while connecting to the server " . $error->getMessage();
          }
     }

     // ID
     public function GetID()
     {
          return $this->ID;
     }

     public function SetID($id)
     {
          $this->ID = $id;
     }

     // Name
     public function GetName()
     {
          return $this->Name;
     }

     public function SetName($name)
     {
          $this->Name = $name;
     }

     // NIM
     public function GetNIM()
     {
          return $this->NIM;
     }

     public function SetNIM($nim)
     {
          $this->NIM = $nim;
     }

     // Email
     public function GetEmail()
     {
          return $this->Email;
     }

     public function SetEmail($email)
     {
          $this->Email = $email;
     }

     // Campus
     public function GetCampus()
     {
          return $this->Campus;
     }

     public function SetCampus($campus)
     {
          $this->Campus = $campus;
     }

     // Programs
     public function GetPrograms()
     {
          return $this->Programs;
     }

     public function SetPrograms($programs)
     {
          $this->Programs = $programs;
     }
}
