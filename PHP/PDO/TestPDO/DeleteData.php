<?php
try {
     $connect = new PDO("mysql:host=localhost; dbname = pegawai", "Billy", "billy19");
     $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

     $result = $connect->query("DELETE FROM pegawai.jabatan WHERE id_jabatan = 4");

     echo $result->rowCount() . " delete data successfully!";
} catch (PDOException $error) {
     die('Connection failed!' . $error->getMessage());
}
