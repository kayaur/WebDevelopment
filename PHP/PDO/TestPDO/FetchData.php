<?php
try {
     $connect = new PDO("mysql:host=localhost; dbname = pegawai", "Billy", "billy19");
     $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

     $result = $connect->query("SELECT * FROM pegawai.jabatan");

     while ($row = $result->fetch()) {
          echo "$row[id_jabatan] | $row[nama_jabatan] <br>";
     }
} catch (PDOException $error) {
     die('Connection failed!' . $error->getMessage());
}
