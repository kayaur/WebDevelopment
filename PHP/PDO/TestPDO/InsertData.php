<?php
try {
     $connect = new PDO("mysql:host=localhost; dbname = pegawai", "Billy", "billy19");
     $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

     $result = $connect->query("INSERT INTO pegawai.jabatan VALUES(NULL, 'Secretary')");

     echo $result->rowCount() . " successfully added in table jabatan";
} catch (PDOException $error) {
     die('Connection failed!' . $error->getMessage());
}
