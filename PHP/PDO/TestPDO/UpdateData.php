<?php
try {
     $connect = new PDO("mysql:host=localhost; dbname = pegawai", "Billy", "billy19");
     $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

     $result = $connect->query("UPDATE pegawai.jabatan SET nama_jabatan = 'Programmer' WHERE id_jabatan = 1");

     echo $result->rowCount() . " update data successfully!";
} catch (PDOException $error) {
     die('Connection failed!' . $error->getMessage());
}
