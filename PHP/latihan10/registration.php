<?php
require 'function.php';

session_start();
if (isset($_SESSION["login"])) {
     header("Location : index.php");
     exit();
}

if (isset($_POST["submit"])) {
     if (register($_POST) > 0) {
          echo "<script>alert ('akun berhasil dibuat');</script>";
     } else {
          echo mysqli_error($conn);
     }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="Loginstyle.css">
     <title>Registration</title>
</head>

<body>
     <h1>Registration</h1>
     <hr>
     <form action="" method="post">
          <div class="container">
               <label for="username"><b>Username</b></label>
               <input type="text" placeholder="Masukan Username" name="username" required>

               <label for="password"><b>Password</b></label>
               <input type="password" placeholder="Masukan Password" name="password" required>

               <label for="password2"><b>Password</b></label>
               <input type="password" placeholder="Konfirmasi Password" name="password2" required>

               <button type="submit" name="submit">Buat Akun</button>
               <label>
                    <input type="checkbox" name="remember"> Remember me
               </label>
          </div>
          <div class="container" style="background-color:#f1f1f1">
               <a href="login.php" class="cancelbtn">Sudah memiliki Akun? Login</a>
               <span class="psw">Forgot <a href="#">password?</a></span>
          </div>
     </form>
</body>

</html>