<?php

$conn = mysqli_connect("localhost", "root", "", "phpbasic");

function query($query)
{
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    return $rows;
}
//add new data
function  AddData($post)
{
    global  $conn;
    //  ambil  data  tiap  elemen
    $nama  =  htmlspecialchars($post["nama"]);
    $nim  =  htmlspecialchars($post["nim"]);
    $jurusan  =  htmlspecialchars($post["jurusan"]);

    //upload  gambar
    $gambar  =  upload();
    if (!$gambar) {
        return  false;
    }

    $query  =  "INSERT  INTO  mahasiswa  VALUES  ('',  '$nama',  '$nim',  '$jurusan', '$gambar')";
    mysqli_query($conn,  $query);

    return  mysqli_affected_rows($conn);
}
//delete data
function delete($id)
{
    global $conn;
    $query = "DELETE FROM mahasiswa WHERE id=$id";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

//update data
function  update($data)
{
    global  $conn;
    $id  =  $data["id"];
    $nama  =  htmlspecialchars($data["nama"]);
    $nim  =  htmlspecialchars($data["nim"]);
    $jurusan  =  htmlspecialchars($data["jurusan"]);

    $gambarLama  =  $data["gambarLama"];
    $error  =  $_FILES["gambar"]["error"];

    //  cek  user  pilih  gambar  atau  tidak if ($error === 4) 
    if ($error === 4) {
        $gambar  =  $gambarLama;
    } else {
        unlink('' . $gambarLama);
        $gambar  =  upload();
    }

    $query  =  "UPDATE  mahasiswa  SET  nama  =  '$nama',  nim  =  '$nim',  jurusan  = '$jurusan',  gambar  =  '$gambar'  WHERE  id  =  $id";
    mysqli_query($conn,  $query);

    return  mysqli_affected_rows($conn);
}

//cari
function cari($keyword)
{
    $query = "SELECT * FROM mahasiswa WHERE nama LIKE '%$keyword%' OR nirm LIKE'%$keyword%' OR jurusan LIKE '%$keyword%'";

    return query($query);
}

function upload()
{
    $namaFile  =  $_FILES["gambar"]["name"];
    $ukuranFile  =  $_FILES["gambar"]["size"];
    $error  =  $_FILES["gambar"]["error"];
    $ext  =  strtolower(pathinfo($namaFile,  PATHINFO_EXTENSION));
    $tmpName  =  $_FILES["gambar"]["tmp_name"];

    //  cek  apakah  ada  gambar  yang 
    if ($error === 4) {
        echo  "<script>
        alert('Gambar  harus  diupload!');
        </script>";
        return  false;
    }

    //  ekstensi  yang  diijinkan
    $extValid  =  ["jpg",  "jpeg",  "png"];
    if (!in_array($ext,  $extValid)) {
        echo  "<script>
        alert('File  yang  diupload  hanya  gambar!');
        </script>";
        return  false;
    }

    // cek ukuran
    if ($ukuranFile  >  1000000) {
        echo  "<script>
        alert('Ukuran  file  terlalu  besar!');
        </script>";
        return  false;
    }

    //  ubah  nama  file  gambar
    $namaBaru  =  uniqid();
    $namaBaru  .=  ".";
    $namaBaru  .=  $ext;
    //  lolos  validasi
    move_uploaded_file($tmpName,  ''  .  $namaBaru);

    return  $namaBaru;
}

//register
function register($data)
{
    global $conn;
    $un = strtolower(stripslashes($data["username"]));
    $pwd = mysqli_real_escape_string($conn, $data["password"]);
    $pwd2 = mysqli_real_escape_string($conn, $data["password2"]);
    // cek un sudah ada atau belum
    $result = mysqli_query($conn, "SELECT username FROM users WHERE username = '$un'");
    if (mysqli_fetch_assoc($result)) {
        echo "<script>
            alert('username sudah ada');
            </script>";
        return false;
    }

    // cek konfirmasi pwd
    if ($pwd !== $pwd2) {
        echo "<script>
            alert('konfirmasi password salah');
            </script>";
        return false;
    }

    // enkripsi pwd
    $pwd = password_hash($pwd, PASSWORD_DEFAULT);

    // tambahkan user ke DB
    mysqli_query($conn, "INSERT INTO users VALUES('', '$un', '$pwd')");
    return mysqli_affected_rows($conn);
}
