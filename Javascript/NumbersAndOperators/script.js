var num = 15.6;

console.log('Floating number:' + num);

console.log(5 * 6);

//increment and decrement
let number = 10;

number++;
console.log('Increment: ' + number);

number--;
console.log('Decrement: ' + number);

number += 40;

console.log(number);

//Bitwise operators
let n = 5 ^ 6;
console.log(n);

let a = 9 | 7;
console.log(a);

let z = 1 & 3;
console.log(z);

let x = 1 < 4;
console.log(x);

let v = 8 > 3;
console.log(v);

//for check the type of variable
document.getElementById("type").innerHTML = typeof 13;
document.getElementById("type1").innerHTML = typeof 'Hello';