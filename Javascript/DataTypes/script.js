let name = { first: 'Billy', second: 'Franscois' }; //object
console.log(name);

let isBool = true; //boolean 
console.log(isBool ? 'True' : 'False');

let lists = [1, 2, 3, 4, 5]; //array

for (let i = 0; i < lists.length; i++) {
     console.log(lists[i]);
}
console.log(lists[3]);

let nothing = null;
console.log(nothing);

let string = 'Yoo', num = 0;
while (num < 5) {
     num++;
     console.log(string);
}

let fruit = 'Orange';
console.log(fruit.length);
console.log(fruit.indexOf('r'));
//console.log(fruit.replace('Grape', '1908'))

function printFruit()
{
     let temp = fruit.slice(1, 6); //to access some index 
     console.log(temp);
     console.log(fruit.toUpperCase(fruit));
     console.log(fruit.toLowerCase(fruit));
}

printFruit();

function Data()
{
     let name = { firstName: 'Billy', lastName: 'Franscois' }; //object

     return name.firstName + ' ' + name.lastName;
}

console.log(Data());