//create function
function show()
{
     console.log('Yoo');
}

//call function
show();

//return function
function input(whatever)
{
     return 'Hello ' + whatever;
}

var whatever = prompt('Enter whatever your want: ');
console.log(input(whatever));

//function with parameter
function calculateNumbers(firstNum, secondNum)
{
     console.log(firstNum * secondNum);
}

calculateNumbers(2, 4); //set the arguments