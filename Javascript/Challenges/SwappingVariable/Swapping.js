const number1 = 19;
const number2 = 8;

function swap(number1, number2)
{
     // With temp variable
     // let temp = number1;

     // number1 = number2;
     // number2 = temp;

     // Without temp variable
     number1 = number1 + number2;
     number2 = number1 - number2;
     number1 = number1 - number2;

     console.log(`After swap: ${number1} and ${number2}`);
}

console.log(`Before swap: ${number1} and ${number2}`);

swap(number1, number2);

// console.log(`After swap: ${number1} and ${number2}`);