const rawPrice = Number(prompt("Enter Raw Price: "));

const vatRate = 20.6 / 100;
const finalPrice = rawPrice * (1 + vatRate);

console.log("The final price is " + finalPrice);