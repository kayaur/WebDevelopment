const temperature = Number(prompt("Enter Temp in Celsius"));

// Celsius to Fahrenheit
function celsiusToFahrenheit(celsius)
{
     return (celsius * 9 / 5) + 32;
}

console.log("Temperature in Celsius: " + temperature);
console.log("Temperature in Fahrenheit: " + celsiusToFahrenheit(temperature));
console.log(`${temperature}°C = ${celsiusToFahrenheit(temperature)}°F`);