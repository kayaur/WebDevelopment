<h1>Personal Website</h1>

My personal website build with HTML, CSS, Sass and Javascript. Hosting on [GitHub Pages](https://pages.github.com/) and integrated database with [Google Firebase](https://firebase.google.com/) cloud.

<i>© Billy Franscois</i>
